export class Article {
    title: string;
    link: string;
    votes: number;

    constructor(title: string, link: string, votes?: number) {
        this.title = title;
        this.link = link;
        this.votes = votes || 0;
    }

    voteUp() {
        this.votes ++;
    }

    voteDown() {
        this.votes --;
    }

    domain(): string {
        try {
            //ex: http://foo.com/path/to/bar
            const domainAndPath: string = this.link.split("//")[1];
            //agora: foo.com/path/to/bar
            return domainAndPath.split('/')[0];
        }
        catch (err) {
            console.log(err);
            return null;
        }
    }
}